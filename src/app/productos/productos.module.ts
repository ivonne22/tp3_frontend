import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductosRoutingModule } from './productos-routing.module';
import { ProductosComponent } from './productos.component';
import {SharedModule} from "../shared/shared.module";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    ProductosComponent
  ],
  exports: [
    ProductosComponent
  ],
    imports: [
        CommonModule,
        ProductosRoutingModule,
        SharedModule,
        MatButtonModule,
        FormsModule
    ],
})
export class ProductosModule { }
