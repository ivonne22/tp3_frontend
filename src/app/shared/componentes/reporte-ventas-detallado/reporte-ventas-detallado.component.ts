import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {VentaCabecera, VentaDetalle} from "../../../models/venta";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {VentaService} from "../../servicios/venta.service";
import {Cliente} from "../../../models/cliente";
import {Producto} from "../../../models/producto";
import {coerceNumberProperty} from "@angular/cdk/coercion";
import {FormControl, FormGroup} from "@angular/forms";

interface Detalle{
  cliente: Cliente,
  fecha: Date,
  producto: Producto,
  cantidad: number,
  totalDetalle: number,
}

@Component({
  selector: 'app-reporte-ventas-detallado',
  templateUrl: './reporte-ventas-detallado.component.html',
  styleUrls: ['./reporte-ventas-detallado.component.css']
})
export class ReporteVentasDetalladoComponent implements OnInit {
  @Input() cabeceras !: VentaCabecera[];

  displayedColumns: string[] = ['producto', 'fecha','cliente','cantidad','totalDetalle'];
  dataSource!: MatTableDataSource<Detalle>;
  clienteVacio: Cliente = {ruc:"",nombreApellido:"",email:""}
  productoVacio: Producto = {precioVenta:0,existencia:0,nombre:"",codigo:0}
  detalleAux: Detalle = {cliente:this.clienteVacio,fecha:new Date(),totalDetalle:0,cantidad:0,producto:this.productoVacio};

  detalles: Detalle[] = [];
  rangoFecha: FormGroup;


  @ViewChild(MatTable) table!: MatTable<VentaCabecera>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    public dialog: MatDialog,
    private servicioCabecera: VentaService,
  ) {
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();

    this.rangoFecha = new FormGroup({
      start: new FormControl(),
      end: new FormControl(),
    });
  }

  ngOnInit(){
    this.cargarDetalles();
    this.dataSource = new MatTableDataSource(this.detalles);
  }

  cargarDetalles(){
    let i=0;
    for(let cabec of this.cabeceras){
      this.detalleAux.cliente = cabec.cliente;
      this.detalleAux.fecha = cabec.fecha;
      for(let det of cabec.detalles){
        this.detalleAux.producto = det.producto;
        this.detalleAux.totalDetalle = det.totalDetalle;
        this.detalleAux.cantidad = det.cantidad;
        this.detalles.push(this.detalleAux);
      }
    }

  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
      return data.producto.nombre.toLowerCase().includes(filter);
    };
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  applyFilterDate(){
    console.log(new Date(this.rangoFecha.value.start))
    console.log(new Date(this.rangoFecha.value.end))
    this.dataSource.data=this.dataSource.data.filter(e=>
      new Date(e.fecha) >= new Date(this.rangoFecha.value.start) &&
      new Date(e.fecha) <= new Date(this.rangoFecha.value.end)
    );
    console.log(this.dataSource)

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
