import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {VentaDetalle} from "../../../models/venta";
import {ProductoService} from "../../servicios/producto.service";
import {Producto} from "../../../models/producto";
import {MatDialog} from "@angular/material/dialog";
import {ModalListarProductoComponent} from "../modal-listar-producto/modal-listar-producto.component";

@Component({
  selector: 'app-formulario-lista-detalle',
  templateUrl: './form-lista-detalle.component.html',
  styleUrls: ['./form-lista-detalle.component.css']
})
export class FormListaDetalleComponent implements OnInit {

  @Output() newItemEvent = new EventEmitter<VentaDetalle>();

  productoVacio:Producto = {codigo:0,nombre:"",precioVenta:0,existencia:0};
  detalle: VentaDetalle = {producto:this.productoVacio,totalDetalle:0,cantidad:0};
  productos!: Producto[];
  producto:Producto = this.productoVacio;
  cantidad!:number;
  totalDetalle!:number;

  constructor(
    private servicioProducto: ProductoService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.productos = this.servicioProducto.getListaProductos();
  }

  agregarDetalle(){
    this.detalle.totalDetalle = this.detalle.producto.precioVenta * this.detalle.cantidad;
    this.newItemEvent.emit(this.detalle);

  }

  abrirModalProducto(){
    this.openDialog();
  }


  openDialog(): void {

    const dialogRef = this.dialog.open(ModalListarProductoComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.detalle.producto = result;
    });

  }


}
