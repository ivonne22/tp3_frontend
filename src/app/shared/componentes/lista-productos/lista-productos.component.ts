import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {Producto} from "../../../models/producto";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {ProductoService} from "../../servicios/producto.service";
import {ModalProductoComponent} from "../modal-producto/modal-producto.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements AfterViewInit {

  @Input() productos !: Producto[];

  displayedColumns: string[] = ['codigo', 'nombre', 'precioVenta', 'existencia', 'editar', 'eliminar'];
  dataSource!: MatTableDataSource<Producto>;

  @ViewChild(MatTable) table!: MatTable<Producto>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  producto:Producto = {codigo:0,nombre:"",precioVenta:0,existencia:0};

  constructor(
    private servicioProducto: ProductoService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(){
    this.dataSource = new MatTableDataSource(this.productos);
  }

  ngOnChanges(){
    this.dataSource = new MatTableDataSource(this.productos);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  eliminarProducto(id: number){
    this.servicioProducto.eliminarProducto(id);
    this.dataSource = new MatTableDataSource(this.servicioProducto.getListaProductos());
  }

  modificarProducto(producto: Producto){
    this.producto.codigo = producto.codigo;
    this.producto.nombre = producto.nombre;
    this.openDialog(producto.nombre,producto.precioVenta,producto.existencia)
  }

  openDialog(nombre:string, precio: number, existencia:number): void {
    const dialogRef = this.dialog.open(ModalProductoComponent, {
      width: '250px',
      data: {nombre: nombre, precioVenta: precio, existencia: existencia, modificar: true},
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.precioVenta == undefined) {
        this.producto.precioVenta = 0;
      } else {
        this.producto.precioVenta = result.precioVenta;
      }
      if(result.existencia == undefined) {
        this.producto.existencia = 0;
      } else {
        this.producto.existencia = result.existencia;
      }
      this.servicioProducto.modificarProducto(this.producto.codigo, this.producto);
      this.dataSource = new MatTableDataSource(this.servicioProducto.getListaProductos());
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
