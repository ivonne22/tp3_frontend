import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {Cliente} from "../../../models/cliente";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import { ClienteService } from '../../servicios/cliente.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalClienteComponent } from '../modal-cliente/modal-cliente.component';

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent implements AfterViewInit {

  @Input() clientes !: Cliente[];

  displayedColumns: string[] = ['ruc', 'nombreApellido', 'email', 'editar', 'eliminar'];
  dataSource!: MatTableDataSource<Cliente>;

  @ViewChild(MatTable) table!: MatTable<Cliente>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  cliente:Cliente = {ruc:"", nombreApellido:"", email:""};

  constructor(
    private servicioCliente: ClienteService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(){
    this.dataSource = new MatTableDataSource(this.clientes);
  }

  ngOnChanges(){
    this.dataSource = new MatTableDataSource(this.clientes);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  eliminarCliente(id: string){
    this.servicioCliente.eliminarCliente(id);
    this.dataSource = new MatTableDataSource(this.servicioCliente.getListaClientes());
  }

  modificarCliente(cliente: Cliente){
    this.cliente.ruc = cliente.ruc;
    this.cliente.nombreApellido = cliente.nombreApellido;
    this.cliente.email = cliente.email;
    this.openDialog(cliente.ruc, cliente.nombreApellido, cliente.email)
  }

  openDialog(ruc:string, nombreApellido: string, email:string): void {
    const dialogRef = this.dialog.open(ModalClienteComponent, {
      width: '250px',
      data: {ruc: ruc, nombreApellido: nombreApellido, email: email, modificar:true},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
      if (result != undefined){
        if(result.nombreApellido == undefined) {
          this.cliente.nombreApellido = "";
        } else {
          this.cliente.nombreApellido = result.nombreApellido;
        }
        if(result.ruc == undefined) {
          this.cliente.ruc = "";
        } else {
          this.cliente.ruc = result.ruc;
        }
        if(result.email == undefined) {
          this.cliente.email = "";
        } else {
          this.cliente.email = result.email;
        }
        this.servicioCliente.modificarCliente(this.cliente.ruc, this.cliente);

      }
      this.dataSource = new MatTableDataSource(this.servicioCliente.getListaClientes());
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
