import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Cliente} from "../../../models/cliente";
import {VentaDetalle} from "../../../models/venta";
import {ClienteService} from "../../servicios/cliente.service";
import {Event} from "@angular/router";

interface Resultado{
  fecha: Date,
  factura: string,
  cliente: Cliente,
  total: number,
  detalles: VentaDetalle[],
  modificar: boolean,
}

@Component({
  selector: 'app-modal-cliente',
  templateUrl: './modal-cabecera.component.html',
  styleUrls: ['./modal-cabecera.component.css']
})
export class ModalCabeceraComponent implements OnInit {

  clientes!: Cliente[];

  constructor(
    public dialogRef: MatDialogRef<ModalCabeceraComponent>,
    private servicioCliente: ClienteService,
    @Inject(MAT_DIALOG_DATA) public data: Resultado,
  ) { }

  cliente!:Cliente;


  ngOnInit(): void {
    this.clientes = this.servicioCliente.getListaClientes();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  cargarDetalles(detalles:VentaDetalle[]){
    console.log(detalles);
    this.data.detalles = detalles;
  }

  cargarTotalDetalle(total:number){
    console.log(total);
    this.data.total = total;
  }

}
