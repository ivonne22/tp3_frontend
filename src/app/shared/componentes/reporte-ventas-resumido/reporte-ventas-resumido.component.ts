import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {VentaCabecera, VentaDetalle} from "../../../models/venta";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {Cliente} from "../../../models/cliente";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {VentaService} from "../../servicios/venta.service";
import {MatDialog} from "@angular/material/dialog";
import {ModalCabeceraComponent} from "../modal-cabecera/modal-cabecera.component";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-reporte-ventas-resumido',
  templateUrl: './reporte-ventas-resumido.component.html',
  styleUrls: ['./reporte-ventas-resumido.component.css']
})
export class ReporteVentasResumidoComponent implements OnInit {
  @Input() cabeceras !: VentaCabecera[];

  displayedColumns: string[] = ['numFactura', 'fecha','cliente','total'];
  dataSource!: MatTableDataSource<VentaCabecera>;
  rangoFecha: FormGroup;


  detalles!: VentaDetalle[];


  @ViewChild(MatTable) table!: MatTable<VentaCabecera>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    public dialog: MatDialog,
    private servicioCabecera: VentaService,
  ) {

    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();

    this.rangoFecha = new FormGroup({
      start: new FormControl(),
      end: new FormControl(),
    });
  }

  ngOnInit(){
    this.dataSource = new MatTableDataSource(this.cabeceras);
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
      return data.cliente.nombreApellido.toLowerCase().includes(filter);
    };
    this.dataSource.filter = filterValue.trim().toLowerCase();
    
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  applyFilterDate(){
    console.log(new Date(this.rangoFecha.value.start))
    console.log(new Date(this.rangoFecha.value.end))
    this.dataSource.data=this.dataSource.data.filter(e=>
      new Date(e.fecha) >= new Date(this.rangoFecha.value.start) &&
      new Date(e.fecha) <= new Date(this.rangoFecha.value.end)
    );
    console.log(this.dataSource)

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
