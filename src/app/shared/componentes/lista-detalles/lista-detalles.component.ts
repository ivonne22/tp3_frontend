import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {VentaCabecera, VentaDetalle} from "../../../models/venta";
import {Cliente} from "../../../models/cliente";
import {Producto} from "../../../models/producto";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {VentaService} from "../../servicios/venta.service";
import {ProductoService} from "../../servicios/producto.service";

@Component({
  selector: 'app-lista-detalles',
  templateUrl: './lista-detalles.component.html',
  styleUrls: ['./lista-detalles.component.css']
})
export class ListaDetallesComponent implements OnInit {

  displayedColumns: string[] = ['producto', 'cantidad','totalDetalle','eliminar'];
  dataSource!: MatTableDataSource<VentaDetalle>;
  detalles: VentaDetalle[] = [];
  productoVacio:Producto = {codigo:0,nombre:"",precioVenta:0,existencia:0};
  detalle: VentaDetalle = {producto:this.productoVacio,totalDetalle:0,cantidad:0};
  productos!: Producto[];
  producto:Producto = this.productoVacio;
  cantidad!:number;
  totalDetalle!:number;
  nuevoDetalle:VentaDetalle={producto:this.productoVacio,totalDetalle:0,cantidad:0}

  @Output() newItemEvent = new EventEmitter<VentaDetalle[]>();
  @Output() total = new EventEmitter<number>();

  @ViewChild(MatTable) table!: MatTable<VentaCabecera>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    public dialog: MatDialog,
    private servicioCabecera: VentaService,
    private servicioProducto: ProductoService,
  ) {

  }

  valorTotal: number = 0;

  ngOnInit(){
    this.dataSource = new MatTableDataSource(this.detalles);
    this.productos = this.servicioProducto.getListaProductos();

  }

  agregarDetalles(){
  }

  ngOnChanges(){
    this.dataSource = new MatTableDataSource(this.detalles);
  }

  agregarDetalle(){
    this.detalle.totalDetalle = this.detalle.producto.precioVenta * this.detalle.cantidad;
    this.nuevoDetalle.producto = this.detalle.producto;
    this.nuevoDetalle.cantidad = this.detalle.cantidad;
    this.nuevoDetalle.totalDetalle = this.detalle.totalDetalle;
    this.valorTotal = this.valorTotal + this.detalle.totalDetalle;
    this.newItemEvent.emit(this.detalles);
    this.total.emit(this.valorTotal);

    this.detalles.push(this.nuevoDetalle);
    this.dataSource = new MatTableDataSource(this.detalles);
  }

  eliminarDetalle(detalle:VentaDetalle){
    for(let i = 0; i <this.detalles.length; i++) {
      if(this.detalles[i] == detalle) {
        this.detalles.splice(i, 1);
      }
    }
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
