import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {Cliente} from "../../../models/cliente";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ClienteService} from "../../servicios/cliente.service";
import {Producto} from "../../../models/producto";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {ProductoService} from "../../servicios/producto.service";
import {ModalProductoComponent} from "../modal-producto/modal-producto.component";

@Component({
  selector: 'app-modal-listar-producto',
  templateUrl: './modal-listar-producto.component.html',
  styleUrls: ['./modal-listar-producto.component.css']
})
export class ModalListarProductoComponent implements OnInit {

  productos: Producto[] = [];
  dataSource!: MatTableDataSource<Producto>;

  constructor(
    public dialogRef: MatDialogRef<ModalListarProductoComponent>,
    private servicioProducto: ProductoService,
    @Inject(MAT_DIALOG_DATA) public data: Producto,
  ) {
    this.productos = this.servicioProducto.getListaProductos();
    this.dataSource = new MatTableDataSource(this.productos);

  }

  ngOnInit(){
  }

  public columns = ['codigo','nombre','precioVenta']

  onNoClick(): void {
    this.dialogRef.close();
  }

  seleccionarProducto(producto:Producto){
    this.data = producto;
  }



}
