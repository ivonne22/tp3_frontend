import { Injectable } from '@angular/core';
import {Cliente} from "../../models/cliente";
import {VentaCabecera, VentaDetalle} from "../../models/venta";
import {MensajeErrorExitoService} from "./mensaje-error-exito.service";
import {Producto} from "../../models/producto";

const CLIENTE_ERNESTO: Cliente = {
  ruc:"123456-0", nombreApellido: "Ernesto Moran", email: "comunacho@gmail.com"
}

const CLIENTE_FERNANDO: Cliente = {
  ruc:"123565-0", nombreApellido: "Fernando Caballero", email: "comunacho@gmail.com"
}

const CLIENTE_NATALIA: Cliente = {
  ruc:"5727525-0", nombreApellido: "Natalia Cardozo", email: "comunacho@gmail.com"
}

const CLIENTE_BEJANMIN: Cliente = {
  ruc:"27257556-0", nombreApellido: "Benjamin racchi", email: "comunacho@gmail.com"
}

const PRODUCTO1: Producto = {
  codigo:5, nombre:"Curita", existencia:50 ,precioVenta:5000
}

const PRODUCTO2: Producto = {
  codigo:3, nombre:"Jabon", existencia:100 ,precioVenta:4000
}

const PRODUCTO3: Producto = {
  codigo:4, nombre:"Mesa", existencia:5 ,precioVenta:200000
}

const PRODUCTO4: Producto = {
  codigo:1, nombre:"Silla", existencia:20 ,precioVenta:80000
}

const PRODUCTO5: Producto = {
  codigo:2, nombre:"Manguera", existencia:3 ,precioVenta:30000
}

const PRODUCTO6: Producto = {
  codigo:6, nombre:"Mochila", existencia:4 ,precioVenta:100000
}

const detalle: VentaDetalle = {producto: PRODUCTO1,totalDetalle: 10000, cantidad: 2}

const DETALLES1: VentaDetalle[] = [
  {producto: PRODUCTO2,totalDetalle: 10000, cantidad: 2},
  {producto: PRODUCTO3,totalDetalle: 12000, cantidad: 3},
]
const DETALLES2: VentaDetalle[] = [
  {producto: PRODUCTO1,totalDetalle: 50000, cantidad: 2},
  {producto: PRODUCTO4,totalDetalle: 40000, cantidad: 5},
]
const DETALLES3: VentaDetalle[] = [
  {producto: PRODUCTO2,totalDetalle: 10000, cantidad: 6},
]
const DETALLES4: VentaDetalle[] = [
  {producto: PRODUCTO2,totalDetalle: 10000, cantidad: 2},
]

const CABECERAS: VentaCabecera[] = [
  {id: 1, fecha: new Date(), numFactura:"001-001-00001", cliente: CLIENTE_FERNANDO,total: 22000, detalles: DETALLES1},
  {id: 2, fecha: new Date(), numFactura:"001-001-00123", cliente: CLIENTE_ERNESTO,total: 50000, detalles: DETALLES2},
  {id: 3, fecha: new Date(), numFactura:"001-001-00265", cliente: CLIENTE_NATALIA,total: 80000, detalles: DETALLES3},
  {id: 4, fecha: new Date(), numFactura:"001-001-00054", cliente: CLIENTE_BEJANMIN,total: 20000, detalles: DETALLES4},
]

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  clienteVacio: Cliente = {ruc:"", email:"", nombreApellido:""}
  productoVacio: Producto = {codigo:0, nombre:"", precioVenta:0, existencia:0}
  clienteAnonimo: Cliente={ruc:"444444",nombreApellido:"SIN NOMBRE", email:""}
  detalles: VentaDetalle[] = [];

  cabecera: VentaCabecera = {id:0,cliente:this.clienteVacio,fecha: new Date(),numFactura:"",total:0,detalles:this.detalles};
  cabeceraVacio: VentaCabecera = {id:0,cliente:this.clienteVacio,fecha: new Date(),numFactura:"",total:0,detalles:this.detalles};
  cabeceras!: VentaCabecera[];
  tamanho!: number;
  contadorCabecera!: number;
  totalCabecera!: number;

  constructor(
    private servicioMensaje: MensajeErrorExitoService) {
    this.cargar();
  }

  cargar(){
    if(localStorage.getItem('cabeceras') === null || localStorage.getItem('cabeceras') == undefined){
      const today = new Date();
      const month = today.getMonth();
      const year = today.getFullYear();
      let cabs = CABECERAS;
      localStorage.setItem('cabeceras',JSON.stringify(cabs));
      this.contadorCabecera = 3;
    }
    return
  }

  setCodigoCabecera(codigo: number){
    this.contadorCabecera = codigo;
  }

  getCodigoCabecera():number{
    return this.contadorCabecera;
  }

  getListaCabeceras(): VentaCabecera[]{
    return JSON.parse(<string>localStorage.getItem('cabeceras'));
  }

  crearCabecera(cabecera: VentaCabecera){
    let cabs = JSON.parse(<string>localStorage.getItem('cabeceras'));
    cabs.push(cabecera);
    localStorage.setItem('cabeceras',JSON.stringify(cabs));
    this.cabecera = this.cabeceraVacio;
  }

  eliminarCabecera(id: number) {
    let cabs = JSON.parse(<string>localStorage.getItem('cabeceras'));

    for(let i = 0; i <cabs.length; i++) {
      if(cabs[i].codigo == id) {
        cabs.splice(i, 1);
      }
    }
    localStorage.setItem('cabeceras', JSON.stringify(cabs));
  }

  modificarCabecera(id: number,cabecera:VentaCabecera) {
    let cabs = JSON.parse(<string>localStorage.getItem('cabeceras'));

    for(let i = 0; i <cabs.length; i++) {
      if(cabs[i].codigo == id) {
        cabs[i] = cabecera;
      }
    }
    localStorage.setItem('cabeceras', JSON.stringify(cabs));
  }
}
