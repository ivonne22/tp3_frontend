import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MensajeErrorExitoService {

  mensajeExito!: string;
  mensajeError!: string;

  constructor() { }

  setMensajeExito(mensaje:string){
    this.mensajeExito = mensaje;
  }

  setMensajeError(mensaje:string){
    this.mensajeError = mensaje;
  }

  getMensajeExito(): string{
    return this.mensajeExito;
  }

  getMensajeError(): string{
    return this.mensajeError;
  }

  clearMensajes(){
    this.mensajeExito="";
    this.mensajeError="";
  }
}
