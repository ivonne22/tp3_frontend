import {Injectable} from '@angular/core';
import {Producto} from "../../models/producto";
import {MensajeErrorExitoService} from "./mensaje-error-exito.service";


@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  producto: Producto = {nombre:"",codigo:0,existencia:0,precioVenta:0};
  productoVacio: Producto = {nombre:"",codigo:0,existencia:0,precioVenta:0};
  productos!: Producto[];
  tamanho!: number;
  contadorProducto!: number;

  constructor(
              private servicioMensaje: MensajeErrorExitoService) {
    this.cargar();
  }

  cargar(){
    if(localStorage.getItem('productos') === null || localStorage.getItem('productos') == undefined){
      let prods = [
        {codigo: 1, nombre: "pizarra", precioVenta: 100000, existencia: 5 },
        {codigo: 2, nombre: "borrador", precioVenta: 20000, existencia: 10 },
        {codigo: 3, nombre: "pincel", precioVenta: 8000, existencia: 50 },
    ];
      localStorage.setItem('productos',JSON.stringify(prods));
      this.contadorProducto = 3;
    }
    return
  }

  setCodigoProducto(codigo: number){
    this.contadorProducto = codigo;
  }

  getCodigoProducto():number{
    return this.contadorProducto;
  }

  getListaProductos(): Producto[]{
    return JSON.parse(<string>localStorage.getItem('productos'));
  }

  crearProducto(codigo: number, nombre:string, precio: number, existencia: number){
    this.producto.codigo = codigo;
    this.producto.nombre = nombre;
    this.producto.precioVenta = precio;
    this.producto.existencia = existencia;
    let prods = JSON.parse(<string>localStorage.getItem('productos'));
    prods.push(this.producto);
    localStorage.setItem('productos',JSON.stringify(prods));
    this.producto = this.productoVacio;
  }

  eliminarProducto(id: number) {
    let prods = JSON.parse(<string>localStorage.getItem('productos'));

    for(let i = 0; i <prods.length; i++) {
      if(prods[i].codigo == id) {
        prods.splice(i, 1);
      }
    }
    localStorage.setItem('productos', JSON.stringify(prods));
  }

  modificarProducto(id: number,producto:Producto) {
    console.log(producto);
    let prods = JSON.parse(<string>localStorage.getItem('productos'));

    for(let i = 0; i <prods.length; i++) {
      if(prods[i].codigo == id) {
        prods[i] = producto;
      }
    }
    localStorage.setItem('productos', JSON.stringify(prods));
  }



}
