import { Component, OnInit } from '@angular/core';
import {ClienteService} from "../shared/servicios/cliente.service";
import {Cliente} from "../models/cliente";
import { MatDialog } from '@angular/material/dialog';
import { ModalClienteComponent } from '../shared/componentes/modal-cliente/modal-cliente.component';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes!: Cliente[];
  ruc!: string;
  nombreApellido!: string;
  email!: string;


  constructor(
    private servicioCliente: ClienteService,
    public dialog: MatDialog,

  ) {
    this.servicioCliente.cargar();
  }


  ngOnInit():void {

    this.clientes = this.servicioCliente.getListaClientes();
  }

  recargarLista(){
    this.clientes = this.servicioCliente.getListaClientes();
  }



  openDialog(): void {
    const dialogRef = this.dialog.open(ModalClienteComponent, {
      width: '250px',
      data: {ruc: "", nombreApellido: "", email: "", modificar:false},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result != undefined){
        if(result.ruc == undefined) {
          this.ruc = "";
        } else {
          this.ruc = result.ruc;
          if(result.nombreApellido == undefined) {
            this.nombreApellido = "";
          } else {
            this.nombreApellido = result.nombreApellido;
          }
          if(result.email == undefined) {
            this.email = "";
          } else {
            this.email = result.email;
          }
          console.log(this.nombreApellido)
          this.servicioCliente.crearCliente(this.ruc,this.nombreApellido,this.email);
        }
      }

      this.recargarLista();
    });
  }

}
