export interface Cliente {
  ruc: string,
  nombreApellido: string,
  email: string
}
