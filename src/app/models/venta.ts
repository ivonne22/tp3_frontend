import {Cliente} from './cliente';
import {Producto} from './producto';

export interface VentaCabecera{
  id: number,
  fecha: Date,
  numFactura: string,
  cliente: Cliente,
  total: number,
  detalles: VentaDetalle[],
}

export interface VentaDetalle{
  producto: Producto,
  cantidad: number,
  totalDetalle: number
}
